#!/bin/sh

python3 ./scripts/100.py 0 50 >> ./logs/0.json &
python3 ./scripts/100.py 51 100 >> ./logs/51.json &
python3 ./scripts/100.py 101 150 >> ./logs/101.json &
python3 ./scripts/100.py 201 250 >> ./logs/201.json &
python3 ./scripts/100.py 251 300 >> ./logs/251.json &
python3 ./scripts/100.py 301 350 >> ./logs/301.json &
python3 ./scripts/100.py 401 450 >> ./logs/401.json &
python3 ./scripts/100.py 451 505 >> ./logs/451.json &

echo "Script Triggered"