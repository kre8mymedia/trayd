class HEADERS:
  API_KEY = 'PK7M95D6NKCFYV4PBOTH'
  SECRET_KEY = 'k3IGycvzjGQ16tfqLRGhLqy6hzX8X9iEKAyFCkPZ'

BASE_URL = 'https://paper-api.alpaca.markets'

web_hook_url = 'https://hooks.slack.com/services/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'

import ta
import time
import json

import requests
import json
import asyncio
import sys

import pandas as pd
from datetime import datetime
from datapackage import Package

import alpaca_trade_api as alpaca
from pandas_datareader import data as web
import yfinance as yfin
yfin.pdr_override()

import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.dates as mdates
from matplotlib.dates import MONDAY, DateFormatter, DayLocator, WeekdayLocator
mpl.style.use('seaborn')

api = alpaca.REST(HEADERS.API_KEY, HEADERS.SECRET_KEY, BASE_URL)

def get_symbols():
  package = Package('https://datahub.io/core/s-and-p-500-companies/datapackage.json')

  symbols = []

  for resource in package.resources:
    if resource.descriptor['datahub']['type'] == 'derived/csv':
      data = resource.read()

  for i in range(0, len(data)):
    symbols.append(data[i][0])

  symbols = [symbol.replace('.', '-') for symbol in symbols]
  # symbols.remove('APC')
  # symbols.remove('BHGE')
  # symbols.remove('APTV')
  # symbols.remove('DWDP')
  # symbols.remove('CSRA')
  # symbols.remove('HRS')
  # symbols.remove('HCP')
  # symbols.remove('LLL')
  # symbols.remove('RHT')
  # symbols.remove('CA')
  # symbols.remove('SCG')
  # symbols.remove('STI')
  # symbols.remove('SYMC')
  # #0022_01162020
  # symbols.remove('HRB')
  # symbols.remove('CHRW')
  # symbols.remove('ES')
  # symbols.remove('NI')
  # symbols.remove('TMK')
  # symbols.remove('TSS')
  # #0933_05092020
  # symbols.remove('BBT')
  # symbols.remove('CELG')
  # symbols.remove('VIAB')

  return symbols

symbols = get_symbols()

# USE THI STO FIND BUGS IN LIST!!
for i in range(0, len(symbols)):
  if symbols[i] == 'EIX':
    print(i)

clock = api.get_clock()
clock = clock._raw
# clock

STOCK_SYMBOL = "SPYD"
SCREEN_WIDTH = 20
START_DATE = '2017-01-01'
END_DATE = clock['timestamp'][:10]
# END_DATE = '2020-06-08'
LOW_RSI = 30
HIGH_RSI = 70

print(END_DATE)
print(f"{END_DATE[:10]} 00:00:00")

def check_position(STOCK_SYMBOL):
  positions = api.list_positions()
  for i in range(0, len(positions)):
    position = positions[i]._raw
    if position['symbol'] == STOCK_SYMBOL:
      if int(position['qty']) > 0:
        return True
      else:
        return False

# check_position(STOCK_SYMBOL)

##################################################
#
#
##################################################
def get_account_info():
  account_info = api.get_account()
  account_info = account_info._raw
  buying_power = account_info['buying_power']
  equity = float(account_info['equity'])
  trade_wallet = round((equity * 0.001), 2)

  account_details = {
      "buying_power": buying_power,
      "equity": equity,
      "trade_wallet": trade_wallet
  }
  return account_details

account = get_account_info()
trade_wallet = account['trade_wallet']
# trade_wallet

##################################################
#
#
##################################################
def get_stock_data(STOCK_SYMBOL, START_DATE, END_DATE):
  df = web.DataReader(STOCK_SYMBOL, data_source='yahoo', start=START_DATE, end=END_DATE)
  df = ta.utils.dropna(df)
  df = ta.add_all_ta_features(df, open='Open', high='High', low='Low', close='Adj Close', volume='Volume', fillna=True)

  signals = []

  for i in range(0, len(df)):
    if df['trend_macd_diff'][i] < 0 and df['momentum_rsi'][i] < LOW_RSI:
      signals.append('buy')
    elif df['trend_macd_diff'][i] > 0 and df['momentum_rsi'][i] > HIGH_RSI:
      signals.append('sell')
    else:
      signals.append('wait')

  df['signals'] = signals

  return df

df = get_stock_data(STOCK_SYMBOL, START_DATE, END_DATE)


##################################################
#
#
##################################################
def get_trade_days(df):
  predictions = []

  # Loop over dataframe
  for i in range(0, len(df)):

    # if buy signal
    if df['signals'][i] == 'buy':

      # if current_open is less than last_open and previous_signal is equal to buy
      if df['Open'][i] < df['Open'][i-1] and df['signals'][i-1] == 'buy':

        # if current_rsi is less than previous_rsi
        if df['momentum_rsi'][i] < df['momentum_rsi'][i-1]:

          #add predictions to predictions to predictions array
          predictions.append(df.iloc[i])

    # if currrent_close is less than previous_close and is equal to sell
    if df['signals'][i] == 'sell':

      if df['trend_macd_diff'][i] > df['trend_macd_diff'][i-1]:
        predictions.append(df.iloc[i])

  predictions = pd.DataFrame(predictions, columns=df.keys())
  return predictions

predictions = get_trade_days(df)

##################################################
#
#
##################################################
def check_for_trades(df, predictions, trade_wallet, STOCK_SYMBOL):
  shares = 0
  dca_cost = 0
  dca_price = 0
  plus_percent = dca_cost * 0.03

  closes = []
  profits = []
  buys = []
  sells = []
  trades = []

  for i in range(0, df.shape[0]):
    if df['signals'][i] == 'wait':
      # print('WAIT | ', df.index[i], 'close:', round(df['Close'][i], 2), 'shares:', shares)
      pass
    if df['signals'][i] == 'buy':
      x = 0
      for x in range(0, predictions.shape[0]):
        if df.index[i] == predictions.index[x]:
          if predictions['signals'][x-1] != 'buy':
            shares += 1
            dca_cost = df['Close'][i]
            dca_price = dca_cost / shares
            trade_wallet -= df['Close'][i]

            # print(' BUY + ', df.index[i], 'close:', round(df['Close'][i], 2), 'shares:', shares, 'dca_cost:', round(dca_cost, 2), 'dca_price:', round(dca_price, 2), 'trade_wallet:', round(trade_wallet, 2), 'BOUGHT')
            my_var = {
              "timstamp": str(df.index[i]),
              "action": "BUY",
              "clsoe": round(df['Close'][i], 2),
              "shares": shares,
              "dca_cost": round(dca_cost, 2),
              "dca_price": round(dca_price, 2),
              "trade_wallet": round(trade_wallet, 2)
            }
            my_var = json.dumps(my_var)
            print(my_var)

            buys.append(df.iloc[i])
            trades.append(df.iloc[i])
            closes.append(df['Close'][i])

            ### BUY COMMAND ###
            if f"{END_DATE[:10]} 00:00:00" == str(df.index[i]):
              # Slack MESSAGE BUY
              message = {'text': f"{clock['timestamp'][:16]} | +BUY+ {STOCK_SYMBOL} @ {round(df['Close'][i], 2)}"}
              requests.post(web_hook_url, data=json.dumps(message))
              print('symbol: ',STOCK_SYMBOL, 'Buying before the close: ', STOCK_SYMBOL)
              # r = api.submit_order(symbol=STOCK_SYMBOL, qty=1, side='buy', type='market', time_in_force='cls')
              # print(r)

          else:
            shares += 1
            closes.append(df['Close'][i])
            dca_cost = sum(closes)
            dca_price = dca_cost / shares
            trade_wallet -= df['Close'][i]
            # print(' DCA ^ ', df.index[i], 'close:', round(df['Close'][i], 2), 'shares:', shares, 'dca_cost:', round(dca_cost, 2), 'dca_price:', round(dca_price, 2), 'trade_wallet:', round(trade_wallet, 2), 'DOUBLE DOWN')
            my_var = {
              "timstamp": str(df.index[i]),
              "action": "DCA",
              "clsoe": round(df['Close'][i], 2),
              "shares": shares,
              "dca_cost": round(dca_cost, 2),
              "dca_price": round(dca_price, 2),
              "trade_wallet": round(trade_wallet, 2)
            }
            my_var = json.dumps(my_var)
            print(my_var)
            buys.append(df.iloc[i])
            trades.append(df.iloc[i])

            ### DCA COMMAND ###
            if f"{END_DATE[:10]} 00:00:00" == str(df.index[i]):
              # SLACK MESSAGE DCA
              message = {'text': f"{clock['timestamp'][:16]} | ++DCA++ {STOCK_SYMBOL} @ {round(df['Close'][i], 2)}"}
              requests.post(web_hook_url, data=json.dumps(message))
              print('symbol: ',STOCK_SYMBOL, 'Buying before the close: ', STOCK_SYMBOL)
              # r = api.submit_order(symbol=STOCK_SYMBOL, qty=1, side='buy', type='market', time_in_force='cls')
              # print(r)

    # if df['signals'][i] == 'sell' or (df['Close'][i] * shares) > ((dca_cost * 0.03) + dca_cost):
    if df['signals'][i] == 'sell':
      x = 0
      for x in range(0, predictions.shape[0]):
        if df.index[i] == predictions.index[x]:
          if predictions['signals'][x-1] != 'sell':
            pred_sell = df['Close'][i]
            pred_sell_total = pred_sell * shares
            profit = pred_sell_total - dca_cost
            profits.append(profit)
            trade_wallet += pred_sell_total

            # print('SELL - ', df.index[i], 'close:', round(df['Close'][i], 2), 'shares:', shares, 'PRED_TOTAL:', round(pred_sell_total, 2), 'PRED_SELL:', round(pred_sell, 2), 'PROFIT:', round(profit, 2), 'TOTAL_PROFIT:', round(sum(profits), 2),  'trade_wallet:', round(trade_wallet, 2), 'SOLD', "\n")
            my_var = {
              "timstamp": str(df.index[i]),
              "action": "SELL",
              "clsoe": round(df['Close'][i], 2),
              "shares": shares,
              "pred_total": round(pred_sell_total, 2),
              "pred_sell": round(pred_sell, 2),
              "profit": round(profit, 2),
              "total_profit": round(sum(profits), 2),
              "trade_wallet": round(trade_wallet, 2)
            }
            my_var = json.dumps(my_var)
            print(my_var)

            sells.append(df.iloc[i])
            trades.append(df.iloc[i])

            ### SELL COMMAND ###
            if f"{END_DATE[:10]} 00:00:00" == str(df.index[i]):
              if check_position(STOCK_SYMBOL) == True:
                print('symbol: ',STOCK_SYMBOL, 'Selling on next open: ', STOCK_SYMBOL)
                positions = api.list_positions()
                for i in range(0, len(positions)):
                  position = positions[i]._raw
                  if position['symbol'] == STOCK_SYMBOL:
                    # SLACK MESSAGE SELL
                    message = {'text': f"{clock['timestamp'][:16]} | --SOLD-- {position['qty']} {STOCK_SYMBOL} @ {round(df['Close'][i], 2)}"}
                    requests.post(web_hook_url, data=json.dumps(message))
                    r = api.submit_order(symbol=STOCK_SYMBOL, qty=position['qty'], side='sell', type='market', time_in_force='gtc')
                    print(r)
              else:
                print('WE DONT OWN SHARES!!')

            shares = 0
            closes = []

          else:
            # print('WAIT | ', df.index[i], 'close:', round(df['Close'][i], 2), 'shares:', shares, 'No Shares')
            pass


  #Array from outputs
  buys = pd.DataFrame(buys, columns=df.keys())
  sells = pd.DataFrame(sells, columns=df.keys())
  trades = pd.DataFrame(trades, columns=df.keys())

  # CLoses wih the BUYs and SELLS
  # plt.figure(figsize=(SCREEN_WIDTH,5))
  # plt.title(f"{STOCK_SYMBOL} Close Price History")
  # plt.plot(df['Close'], label='Close')
  # plt.plot(buys['Close'], label='Buys', marker="o")
  # plt.plot(sells['Close'], label='Sells', marker="o")
  # plt.plot(trades['Close'], label='Trades', linestyle=':', color="black")
  # plt.legend()

  # # MACD Chart
  # plt.figure(figsize=(SCREEN_WIDTH,5))
  # plt.plot(df.trend_macd, label='MACD')
  # plt.plot(df.trend_macd_signal, label='MACD Signal')
  # plt.plot(df.trend_macd_diff, label='MACD Difference')
  # plt.axhline(y=0, linestyle=':',color='black')
  # plt.title('MACD, MACD Signal and MACD Difference')
  # plt.legend()

  # # Volume Chart
  # plt.figure(figsize=(SCREEN_WIDTH,2))
  # plt.title('Volume')
  # plt.plot(df['Volume'], color='m')

  # # RSI Chart
  # plt.figure(figsize=(SCREEN_WIDTH,3))
  # plt.title('RSI')
  # plt.plot(df['momentum_rsi'], label='RSI', color='purple')
  # plt.axhline(y=HIGH_RSI, color='red')
  # plt.axhline(y=LOW_RSI, color='green')
  # plt.show()

# UNCOMMENT LINE BELOW TO RUN ALGO ON SPECIFIED COMPANY
# check_for_trades(df, predictions, trade_wallet, STOCK_SYMBOL)

##################################################
#
#
##################################################
symbols = get_symbols()

def app(symbols, START_DATE, END_DATE):
  account = get_account_info()
  equity = account['equity']
  trade_wallet = account['trade_wallet']

  for i in range( int(sys.argv[1]), int(sys.argv[2]) ):
    stock_symbol = symbols[i]
    print("\n", 'STOCK: ', stock_symbol)

    try:
      df = get_stock_data(stock_symbol, START_DATE, END_DATE)
      predictions = get_trade_days(df)
      check_for_trades(df, predictions, trade_wallet, stock_symbol)
    except Exception as e:
      print("\n ERROR: ", e)
      pass

app(symbols, START_DATE, END_DATE)